const clientes =[
  {id:1,nombre:'Leonor'},
  {id:2,nombre:'Jacinto'},
  {id:3,nombre:'Waldo'}
];
const pagos = [
  {id:1,pago:'1000',moneda:'Bs'},
  {id:2,pago:'1800',moneda:'Bs'}
];

const id = 2;
const getCliente = (id) =>{
 return new Promise ((resolve,reject)=>{
   const cliente = clientes.find(cli => cli.id ===id );
   if (cliente){
     resolve(cliente);
   } else {
     reject (`No existe el cliente con el id ${id}`);
   }
 });
};

const getPago = (id) =>{
  return new Promise ((resolve,reject)=>{
    const pago = pagos.find(pag=>pag.id===id);
    if(pago){
      resolve(pago);
    }else{
      reject(`No existe el salario con el id ${id}`);
    }
  });
}

let nombre;
let ide;

getCliente (id)
.then(cliente =>{
  console.log(cliente);
  nombre=cliente.nombre;
  ide=cliente.id;
  return getPago(id);
})
.then(pago=>{
  console.log('El empleado:',nombre, '\nid:',ide,'\n','Id:',pago['id'],'\nPago:',pago['pago'],'\nMoneda:',pago['moneda']);
})
.catch(error =>{
  console.log(error);
})