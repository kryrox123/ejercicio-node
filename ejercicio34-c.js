const clientes =[
  {id:1,nombre:'Leonor'},
  {id:2,nombre:'Jacinto'},
  {id:3,nombre:'Waldo'}
];
const pagos = [
  {id:1,pago:'1000',moneda:'Bs'},
  {id:2,pago:'1800',moneda:'Bs'}
];

// c)Promesa
const id = 2;
const getCliente = (id) =>{
 return new Promise ((resolve,reject)=>{
   const cliente = clientes.find(cli => cli.id ===id );
   if (cliente){
     resolve(`-Datos- \nid: ${cliente.id} \nnombre: ${cliente.nombre}`);
   } else {
     reject (`No existe el empleado con el id ${id}`);
   }
 });
};

const getPago = (id) =>{
  return new Promise ((resolve,reject)=>{
    const pago = pagos.find(pag=>pag.id===id);
    if(pago){
      resolve(`-Pagos- \nid: ${pago.id} \npago: ${pago.pago} \nmoneda: ${pago.moneda}`);
    }else{
      reject(`No existe el salario con el id ${id}`);
    }
  });
}

getCliente(id)
.then(cliente =>console.log(cliente))
.catch(error=>{
  console.log(error);
});

 getPago(id)
 .then(pago=>console.log(pago))
 .catch(error=>{
  console.log(error);
});